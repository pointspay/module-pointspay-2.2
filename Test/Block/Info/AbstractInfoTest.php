<?php
namespace Pointspay\Pointspay\Test\Block\Info;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Block\Info\AbstractInfo;

class AbstractInfoTest extends TestCase {
    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
