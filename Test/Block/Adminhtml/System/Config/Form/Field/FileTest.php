<?php
namespace Pointspay\Pointspay\Test\Block\Adminhtml\System\Config\Form\Field;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Block\Adminhtml\System\Config\Form\Field\File;

class FileTest extends TestCase {
    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
