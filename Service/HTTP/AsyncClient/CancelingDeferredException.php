<?php
namespace Pointspay\Pointspay\Service\HTTP\AsyncClient;

/**
 * Exception related to canceling a deferred operation.
 */
class CancelingDeferredException extends \RuntimeException
{

}
