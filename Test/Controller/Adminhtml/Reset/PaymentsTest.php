<?php
namespace Pointspay\Pointspay\Test\Controller\Adminhtml\Reset;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Controller\Adminhtml\Reset\Payments;

class PaymentsTest extends TestCase {
    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
