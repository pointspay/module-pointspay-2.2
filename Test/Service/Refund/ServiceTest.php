<?php
namespace Pointspay\Pointspay\Test\Service\Refund;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Service\Refund\Service;

class ServiceTest extends TestCase {
    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
