<?php
namespace Pointspay\Pointspay\Service\HTTP\AsyncClient;


/**
 * Described deferred operation that can be canceled.
 *
 * @api
 */
interface CancelableDeferredInterface extends DeferredInterface
{
    /**
     * Cancels the operation.
     *
     * Will not cancel the operation when it has already started and given $force is not true.
     *
     * @param bool $force Cancel operation even if it's already started.
     * @return void
     * @throws CancelingDeferredException When failed to cancel.
     */
    public function cancel(bool $force = false);

    /**
     * Whether the operation has been cancelled already.
     *
     * @return bool
     */
    public function isCancelled(): bool;
}
