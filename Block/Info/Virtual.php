<?php


namespace Pointspay\Pointspay\Block\Info;

class Virtual extends AbstractInfo
{
    /**
     * @var string
     */
    protected $_template = 'Pointspay_Pointspay::info/virtual.phtml';
}
