<?php
namespace Pointspay\Pointspay\Test\Block\Payment\Block\Form;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Block\Payment\Block\Form\ContainerPlugin;

class ContainerPluginTest extends TestCase {
    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
