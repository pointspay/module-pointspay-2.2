<?php
namespace Pointspay\Pointspay\Service\HTTP\AsyncClient;


/**
 * Failed to send HTTP request.
 */
class HttpException extends \RuntimeException
{

}
