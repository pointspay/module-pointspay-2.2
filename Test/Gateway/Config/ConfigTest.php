<?php
namespace Pointspay\Pointspay\Test\Gateway\Config;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Gateway\Config\Config;

class ConfigTest extends TestCase {
    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
