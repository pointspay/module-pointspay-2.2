<?php
namespace Pointspay\Pointspay\Test\Block\Adminhtml\System\Config;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Block\Adminhtml\System\Config\Fieldset;

class FieldsetTest extends TestCase {

    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
