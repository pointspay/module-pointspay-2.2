<?php
namespace Pointspay\Pointspay\Api;

use Pointspay\Pointspay\Service\HTTP\AsyncClient\HttpResponseDeferredInterface;
use Pointspay\Pointspay\Service\HTTP\AsyncClient\Request;

/**
 * Asynchronous HTTP client.
 *
 * @api
 */
interface AsyncClientInterface
{
    /**
     * Perform an HTTP request.
     *
     * @param Request $request
     * @return HttpResponseDeferredInterface
     */
    public function request(Request $request): HttpResponseDeferredInterface;
}
