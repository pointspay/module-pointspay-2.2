<?php
namespace Pointspay\Pointspay\Test\Block\Checkout\LayoutProcessor;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Block\Checkout\LayoutProcessor\BillingAddressClonerProcessor;

class BillingAddressClonerProcessorTest extends TestCase {
    // there is no test yet. only stub to pass the build. no impact on the code coverage
    public function testStub()
    {
        $this->assertTrue(true);
    }
}
