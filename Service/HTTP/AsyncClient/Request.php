<?php

namespace Pointspay\Pointspay\Service\HTTP\AsyncClient;

class Request
{
    const METHOD_GET = 'GET';

    const METHOD_POST = 'POST';

    const METHOD_HEAD = 'HEAD';

    const METHOD_PUT = 'PUT';

    const METHOD_DELETE = 'DELETE';

    const METHOD_CONNECT = 'CONNECT';

    const METHOD_PATCH = 'PATCH';

    const METHOD_OPTIONS = 'OPTIONS';

    const METHOD_PROPFIND = 'PROPFIND';

    const METHOD_TRACE = 'TRACE';

    private $options;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string[]
     */
    private $headers;

    /**
     * @var string|null
     */
    private $body;

    public function __construct(string $url, string $method, array $headers, string $body = null, array $options = [])
    {
        $this->url = $url;
        $this->method = $method;
        $this->headers = $headers;
        $this->body = $body;
        $this->options = $options;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * URL to send request to.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * HTTP method to use.
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Headers to send.
     *
     * Keys - header names, values - array of header values.
     *
     * @return string[][]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Body to send
     *
     * @return string|null
     */
    public function getBody()
    {
        return $this->body;
    }
}
