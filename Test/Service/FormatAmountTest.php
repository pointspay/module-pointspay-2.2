<?php

namespace Pointspay\Pointspay\Test\Service;

use PHPUnit\Framework\TestCase;
use Pointspay\Pointspay\Service\FormatAmount;

class FormatAmountTest extends TestCase
{
    private $formatAmount;

    protected function setUp()
    {
        $this->formatAmount = new FormatAmount();
    }

    public function testFormatAmountReturnsExpectedValue()
    {
        $this->assertEquals(100, $this->formatAmount->formatAmount(1, 'USD'));
    }

    public function testDecimalNumbersReturnsExpectedValue()
    {
        $this->assertEquals(2, $this->formatAmount->decimalNumbers('USD'));
    }

    public function testOriginalAmountReturnsExpectedValue()
    {
        $this->assertEquals(1, $this->formatAmount->originalAmount(100, 'USD'));
    }

    public function testFormatAmountHandlesZeroDecimals()
    {
        $this->assertEquals(1, $this->formatAmount->formatAmount(1, 'JPY'));
    }

    public function testDecimalNumbersHandlesZeroDecimals()
    {
        $this->assertEquals(0, $this->formatAmount->decimalNumbers('JPY'));
    }

    public function testOriginalAmountHandlesZeroDecimals()
    {
        $this->assertEquals(1, $this->formatAmount->originalAmount(1, 'JPY'));
    }

    public function testFormatAmountHandlesThreeDecimals()
    {
        $this->assertEquals(1000, $this->formatAmount->formatAmount(1, 'BHD'));
    }

    public function testDecimalNumbersHandlesThreeDecimals()
    {
        $this->assertEquals(3, $this->formatAmount->decimalNumbers('BHD'));
    }

    public function testOriginalAmountHandlesThreeDecimals()
    {
        $this->assertEquals(1, $this->formatAmount->originalAmount(1000, 'BHD'));
    }

    public function testFormatAmountHandlesTwoDecimals()
    {
        $this->assertEquals(100, $this->formatAmount->formatAmount(1, 'MRO'));
    }
    public function testOriginalAmountHandlesTwoDecimals()
    {
        $this->assertEquals(1, $this->formatAmount->originalAmount(1000, 'BHD'));
    }
}
